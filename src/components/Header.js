import React, { Component } from 'react';

export  default class Header extends Component {
    constructor(props) {
        super(props)
         
      }
      render() {
            return (
                <div>
                <nav className="navbar navbar-default">
                    <div className="container">
                        <div className="navbar-header">
                            <ul className="nav navbar-nav">
                                <li><a href="/donator">For Donators</a></li>
                                <li><a href="/">For Patients</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                </div>
            );
      }
}
import React, { Component } from 'react';
import Header from './Header';
import { listeningForNewBloodDonators, emitBloodDonatorInformationToTheServer } from '../api';
import Select from 'react-select';
import { Scene, Popup, Layers, Symbols, Geometry, Graphic,Widgets  } from 'react-arcgis';
import marker from './marker';
const SearchWidget = Widgets.Search;

export  default class Patient extends Component {
    constructor(props) {
        super(props)
      
             
            this.state = {
                      _id :'',
                      bloodDonators: [],
                      latitude: null,
                      longitude: null,
                      firstName: '',
                      lastName: '',
                      contactNumber : '',
                      email: '',
                      bloodGroup:'',
                      bloodOptions: [],
                      clientIp:'',
                      x: 0, y: 0, // mouse coordinates on screen
                      countryName :'',
                      city:'',
                      countryCode:'',
                      openForm: false,
                      centerMap: [],
                      scaleMap: 30000
                      
              
          }
      }
      cleanState = () => {
        
         this.setState({_id: ''});    
         this.setState({latitude: 0});
         this.setState({longitude: 0});
         this.setState({firstName: ''});
         this.setState({lastName: ''});
         this.setState({contactNumber: ''});
         this.setState({email: ''});
         this.setState({bloodGroup: ''});
         this.setState({clientIp: ''});
         this.setState({x: 0});
         this.setState({y: 0});
         this.setState({countryName: ''});
         this.setState({city: ''});
         this.setState({countryCode: ''});
         this.setState({openForm: false});
        
        }

      getCurrentAreaInformationBasedOnIp(){
        
            fetch('https://freegeoip.net/json/').then((response) => response.json()).then((responseJson) => {
        
                  if(!responseJson.error)
                  {
                    this.setState({clientIp : responseJson.ip});
                    this.setState({countryName : responseJson.country_name});
                    this.setState({countryCode : responseJson.country_code});
                    this.setState({city : responseJson.city});
                  }
        
                  this.getBloodDonorsInfoInCurrentArea(this.state);
              });
        
          }

          getBloodDonorsInfoInCurrentArea(address){
            
               // post ajax                /bloodDonors/saveInfo
               fetch('http://localhost:3005/bloodDonators/getBloodDonators', {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({            
                  countryName : address.countryName,            
                  city : address.city
                })
              })
              .then((response) => response.json())
              .then((responseJson) => {
               
                if(!responseJson.error){
                 
                  this.setState({bloodDonators : responseJson.donators});
                   //console.log("\ndonators from server: " + JSON.stringify( responseJson.donators));
                }else
               {
                console.error(responseJson.message);
               }
               // console.log("responseJSON from server: " + JSON.stringify( responseJson));
               // console.log("state after cleaning: " + JSON.stringify( this.state));
              })
              .catch((error) => {
                console.error(error);
              });
        
          }

          handleClickEvent(data){
            let bloodDonators = this.state.bloodDonators;
        
            let indexBloodDonator = bloodDonators.findIndex( x=>x.address.latitude.toFixed(3) === data.mapPoint.latitude.toFixed(3)
             && x.address.longitude.toFixed(3) === data.mapPoint.longitude.toFixed(3) );
        
        
             if(indexBloodDonator > -1)
             {
             
              console.log("blood Donator found: "+bloodDonators[indexBloodDonator]);
              return bloodDonators[indexBloodDonator]
        
             }
             else{
               return -1;
             }
            
        
          }

          componentDidMount () {
            
            
            listeningForNewBloodDonators( (err, bloodDonator) => {
            
               if(err)
               {
                 alert("error listening from server");
               }else{
       
                 let donators = this.state.bloodDonators;
       
                  //first we control if this bloodDonator is new or existed before!
       
                  let indexBloodDonator = donators.findIndex( x=>x._id === bloodDonator._id );
       
                  if(indexBloodDonator === -1)
                  {
                   donators.push(bloodDonator);
                  }
                  
                  if(indexBloodDonator > -1)
                  {
                    // this bloodDonator info was updated
                     
       
                    if(bloodDonator.isDeleted)
                     {
                      //// we shoul eleminate it from the array! Did before
                      // and do not add new bloodDonator received from the server!
                      donators.splice(indexBloodDonator,1);
                     }
                     else{
                       donators.splice(indexBloodDonator,1);
                       donators.push(bloodDonator);
                     }
                  
       
                    console.log("donators after slicing: " + JSON.stringify(donators ));
       
                   
                  }
       
                 this.setState({bloodDonators : donators});
               }
            });
           //  console.log("state after broadcasting: " + JSON.stringify( this.state));
            // this.listenSocketIoBroadcasting();
       
            this.getCurrentAreaInformationBasedOnIp();   
       
           }

           componentWillMount() {
            
                
                  fetch('https://freegeoip.net/json/').then((response) => response.json()).then((responseJson) => {
                    
                          if(!responseJson.error)
                          {
                            this.setState({latitude : responseJson.latitude});
                            this.setState({longitude : responseJson.longitude});
                            this.setState({countryName : responseJson.country_name});
                            this.setState({city : responseJson.city});
                            //console.log("ip data: " + JSON.stringify(responseJson));
                          }
                        
                  });
            
               
               this.getCurrentAreaInformationBasedOnIp(); 
            
               let bloodOptions = [{ value: '0+', label: '0+' },{ value: '0-', label: '0-' }, { value: 'A+', label: 'A+' }, { value: 'A-', label: 'A-' }, { value: 'B+', label: 'B+' }, { value: 'B-', label: 'B-' }, { value: 'AB+', label: 'AB+' }, { value: 'AB-', label: 'AB-' },];
             
                this.setState({
                  bloodOptions : bloodOptions
                })
            
                ////console.log("rendered component.\nstudents: "+ JSON.stringify(students ) +"\n blodOptions: "+JSON.stringify(bloodOptions));
              }
            
              handleMouseWheel(e) {
                // e.stopPropagation();
                 this.setState({                    
                         scaleMap: this.state.scaleMap + (e.deltaY * (this.state.scaleMap / 250)) 
                 });
             
                 console.log("map scale: "+ this.state.scaleMap);
               }
      render() {


        let markers = this.state.bloodDonators.map( marker =>{
            
                    return(
                      <Graphic key={marker._id}  onClick={() => {console.log('clicked the Graphic!')}} >
                         <Symbols.SimpleMarkerSymbol
                                symbolProperties={{
                                    color: [226, 119, 40],
                                    outline: {
                                        color: [255, 255, 255],
                                        width: 1
                                    }
                                }}
                            />
            
                            <Geometry.Point
                                  
                                    geometryProperties={{
                                        latitude: marker.address.latitude,
                                        longitude: marker.address.longitude
                                    }}
            
                                    onClick={() => {console.log('clicked the Point!')}}
                                   
                                  />
                     
                      </Graphic>
                    );
            
                  });

        return ( 
            <div>
                <Header />
                {
                    this.state.openForm && 
                    <div id="myModal" style={{left: this.state.x, top: this.state.y}} className="addUserPopup modal fade" role="dialog">
                                <input id="bloodDonatorInfoId" type="hidden" value={ this.state._id } />
                                <div className="modal-dialog">
                                <div className="modal-content">
                                 <div className="modal-header">
          
                                    Location Selected:[&nbsp;lat:&nbsp;{this.state.latitude.toFixed(5)},&nbsp;lon:&nbsp;{this.state.longitude.toFixed(5)}]<br/>
                                    
                                 </div>
                    
                                <label htmlFor="firstNameTXT" className="control-label">Insert first name</label>
                                <input disabled type="text" id="firstNameTXT" ref="firstNameTXT"  value={this.state.firstName} onChange={this.handleChangeFirstName} /><br/>
                                <label htmlFor="lastNameTXT"   className="control-label">Insert last name</label>
                                <input disabled type="text" id="lastNameTXT" ref="lastNameTXT"  value={this.state.lastName} onChange={this.handleChangeLastName} /><br/>
                                <label htmlFor="contactNumberTXT" className="control-label">Contact Number</label>
                                <input disabled type="text" id="contactNumberTXT" ref="contactNumberTXT" value={this.state.contactNumber} onChange={this.handleChangeContactNumber} /><br/>
                                <label id="lblBloodType" htmlFor="bloodTypeTXT" className="control-label">Blood Type  </label>
                                <Select id="selectBloodDDL" value={this.state.bloodGroup}   name="form-field-name"  options={this.state.bloodOptions}  onChange={this.bloodChange} />
                                <label htmlFor="emailAddressTXT" className="control-label">Email</label>
                                <input disabled type="text" id="emailAddressTXT" ref="emailAddressTXT" value={this.state.email} onChange={this.handleChangeEmailAddress} /><br/>
                                <span  />
                               
                                 <button id="cancelInfo" onClick={this.cleanState}>Cancel</button>                               
                               
                                </div>
                               </div>
                    
                              </div>
                }
                <Scene
                      
                                  style={{ width: '100vw', height: '100vh' }}
                      
                                  mapProperties={{ basemap: 'satellite' }}
                               
                                  viewProperties={{
                                      center: [ this.state.longitude,this.state.latitude],
                                      zoom: 30,
                                      scale: this.state.scaleMap
                                  } }
                                 
                                  onClick={(data) => {
                                   
                                   var result = this.handleClickEvent(data);
                      
                                   if(result === -1)
                                    {
                                      /*first we clean the old state
                                        then we set it with new data selected from the map!
                                      */
                                      this.cleanForm();
                                      this.setState({
                                        openForm: true,
                                        x: data.x,
                                        y: data.y,
                                        latitude: data.mapPoint.latitude,
                                        longitude: data.mapPoint.longitude
                                      });
                                    }
                                    else{
                                      /* this is a point in the map where another bloodDonatorInfo exists!
                                         so instead of opening a new clean form we open the form with that data
                                      */
                                      this.setState({
                                        _id : result._id,
                                        openForm: true,
                                        x: data.x,
                                        y: data.y,
                                        latitude: result.address.latitude,
                                        longitude: result.address.longitude,
                                        firstName: result.firstName,
                                        lastName: result.lastName,
                                        contactNumber: result.contactNumber,
                                        email: result.email,
                                        bloodGroup: result.bloodGroup
                                      });
                                    }
                                   
                                 // console.log("this.state on Click: "+JSON.stringify(this.state))  
                              
                                }}
                              >
                             
                      
                              <Layers.GraphicsLayer onClick={() => {console.log('clicked the GraphicsLayer!')}} >        
                                    {markers} 
                              </Layers.GraphicsLayer>
                      
                              <SearchWidget position="top-right" />
                              
                              </Scene>
               
            </div>
          );
    }
}
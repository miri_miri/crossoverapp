import openSocket from 'socket.io-client';

// const  socket = openSocket('http://localhost:8000');
const  socket = openSocket('http://localhost:3005').connect();

function listeningForNewBloodDonators(cb) {
  
    socket.on("BloodDonatorInfoServer", bloodDonators => cb(null, bloodDonators));
  
  // socket.emit('subscribeToTimer', 1000);
  // socket.on('showThis',function(msg){
  // alert(msg);
  // });

}

function emitBloodDonatorInformationToTheServer(bloodDonator){


    console.log("bloodDonator to be sent on server: " + JSON.stringify(bloodDonator));

    socket.emit("newBloodDonator",{
        _id : bloodDonator._id,
        firstName: bloodDonator.firstName,
        lastName: bloodDonator.lastName,
        email :bloodDonator.email,
        contactNumber : bloodDonator.contactNumber,
        bloodGroup : bloodDonator.bloodGroup,
       address : {
        latitude: bloodDonator.latitude,
        longitude:bloodDonator.longitude
       },      
       link: bloodDonator.link,
       isDeleted:bloodDonator.isDeleted || false
    });

}
export { socket,listeningForNewBloodDonators,emitBloodDonatorInformationToTheServer };
import React from 'react';
import Donator from '../components/Donator';
import Patients from '../components/Patient';
import {
    BrowserRouter ,
    Route ,
    Switch 
} from 'react-router-dom';




export  default () => (
    <BrowserRouter>
      <Switch>
         <Route path="/donator" component={Donator} />
         <Route path="/" component={Patients} />
       </Switch>
    </BrowserRouter>
)
                
      